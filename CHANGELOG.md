# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.0] - 2022-03-18
### Added
- GameExiter to exit game.
- Add VariablesSO that also raise event on change.
- Auto setter for transform anchor

### Changed
- SceneLoadRequester can now register to event
- Anchor base update

### Fixed
- Remove unused using directives
- Fix small error when onplay with addressables


## [1.1.1] - 2021-10-28
### Fix
- Licensing stuff


## [1.1.0] - 2021-10-28
### Changed
- Add `NaughtyAttributes` as a requirement to use this library.
- SOWorflow: Move RuntimeAnchors logic into generic class.
- SceneManagement: Prevent error when not using PathTaken.


## [1.0.1] - 2021-10-28
### Changed
- SOWorflow: Make Bool & GameObject channels inherit from BaseSO.
- StateMachine: Change transition editor menu title.

### Fixed
- StateMachine: Actions and Conditions creation using template.
- StateMachine: Add description field for Actions and Conditions.


## [1.0.0] - 2021-10-12
First release of the package. Changes made include: 
- Adding StateMachines namespace.
- Using assembly definition files
- Remonving unused or specific scripts.
- Folder reorganisation.
- Tweaks on SceneManagement, to make it more generic.
- Adding StateMachineActionSO.
- Adding README files for each folder.
