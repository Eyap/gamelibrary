# SOWorkflowCommon

A library package that contains common used scripts to build an app using the ScriptableObjects (SO) workflow.

## Dependencies

This package needs :

- The `NaughtyAttributes` package made by (Denis Rizov)[https://github.com/dbrizov]. You can add it in PM via git url with `https://github.com/dbrizov/NaughtyAttributes.git#upm`.
