﻿namespace GameLibrary.SOWorkflowCommon.RuntimeAnchors
{
    using NaughtyAttributes;
    using UnityEngine;
    using UnityEngine.Events;

	public abstract class RuntimeAnchorBase<T> : DescriptionBaseSO
    {
        public UnityAction OnAnchorProvided;

        [Header("Debug")]
        [ReadOnly] public bool isSet = false; // Any script can check if the transform is null before using it, by just checking this bool

        [ReadOnly] [SerializeField] private T _value;
		public T value
        {
            get { return _value; }
        }

        public void Provide(T value)
        {
            if (value == null)
            {
                Debug.LogError("A null value was provided to the " + this.name + " runtime anchor.");
                return;
            }

            _value = value;
            isSet = true;

            if (OnAnchorProvided != null)
                OnAnchorProvided.Invoke();
        }

        public void Unset()
        {
			_value = default(T);
            isSet = false;
        }

        private void OnDisable()
        {
            Unset();
        }
    }
}
