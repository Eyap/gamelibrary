﻿namespace GameLibrary.SceneManagement
{
	using GameLibrary.SOWorkflowCommon.Events;
	using UnityEngine;

	public class GameExiter : MonoBehaviour
	{
		[SerializeField] private VoidEventChannelSO _onExitGameButton = default;

		private void Start()
		{
			_onExitGameButton.OnEventRaised += ExitGame;
		}

		private void OnDestroy()
		{
			_onExitGameButton.OnEventRaised -= ExitGame;
		}

		public void ExitGame()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#endif
			Application.Quit();
		}
	}
}
