namespace GameLibrary.SceneManagement
{
	using UnityEngine;

	/// <summary>
	/// This class contains Settings specific to Menus only
	/// </summary>
	[CreateAssetMenu(fileName = "NewMenu", menuName = "GameLibrary/Scene Management/Menu SO")]
	public class MenuSO : SceneSO { }
}
