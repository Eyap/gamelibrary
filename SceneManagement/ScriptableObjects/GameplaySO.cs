namespace GameLibrary.SceneManagement
{
	using UnityEngine;

	/// <summary>
	/// This class contains Settings specific to Gameplay scenes only
	/// </summary>

	[CreateAssetMenu(fileName = "NewGameplayScene", menuName = "GameLibrary/Scene Management/Gameplay SO")]

	public class GameplaySO : SceneSO { }
}
