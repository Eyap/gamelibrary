namespace GameLibrary.SceneManagement
{
	using UnityEngine;

	/// <summary>
	/// The scene data for the persistent managers.
	/// </summary>
	[CreateAssetMenu(fileName = "PersistentManagers", menuName = "GameLibrary/Scene Management/Persistent Managers")]
	public class PersistentManagersSO : SceneSO { }
}
