namespace GameLibrary.SceneManagement
{
	using GameLibrary.SOWorkflowCommon;
    using NaughtyAttributes;
    using UnityEngine;

	/// <summary>
	/// This one-of-a-kind-SO stores, during gameplay, the path that was used last (i.e. the one that was taken to get to the current scene).
	/// Only one PathStorageSO is needed.   
	/// </summary>
	[CreateAssetMenu(fileName = "PathStorage", menuName = "GameLibrary/Scene Management/Path Storage")]
    public class PathStorageSO : DescriptionBaseSO
	{
        [ReadOnly] public PathSO lastPathTaken;
	}
}
