# SceneManagement

A system to build an app that relies on different scenes, including menu and gameplay scenes.
It is based on the open source project 1 of Unity, Chop Chop.

## Dependencies

This package has a few external dependencies that you will need to install first (it can alos be done after import, but will fill the console with errors in the meantime) :

- the Addressables unity package. You can find it in the PackageManager.
- the Localization package. You can add it with `com.unity.localization`

To use this package, you must also use the SOWorkflowCommon Library.
