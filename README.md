# GameLibrary

My game library to share common workflow and cool stuff between games and projects.

## Dependencies

This library has a few external dependencies that you will need to install first (it can also be done after import, but will fill the console with errors in the meantime) :

- the Addressables unity package. You can find it in the PackageManager (PM).
- the Localization package. You can add it in PM with `com.unity.localization`.
- The `NaughtyAttributes` package made by [Denis Rizov](https://github.com/dbrizov). You can add it in PM via git url with `https://github.com/dbrizov/NaughtyAttributes.git#upm`.

## License

Initial work made by [UnityTechnologies](https://github.com/UnityTechnologies), provided under the Apache License 2.0, see [COPY](./COPY) file.
You will find the list of changes in the [CHANGELOG](./CHANGELOG) file.

All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
