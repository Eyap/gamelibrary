using UnityEngine;
using GameLibrary.StateMachines;
using GameLibrary.StateMachines.ScriptableObjects;

[CreateAssetMenu(fileName = "#RUNTIMENAME#", menuName = "GameLibrary/State Machines/Actions/#RUNTIMENAME_WITH_SPACES#")]
public class #SCRIPTNAME# : StateActionSO
{
	protected override StateAction CreateAction() => new #RUNTIMENAME#();
}

public class #RUNTIMENAME# : StateAction
{
	protected new #SCRIPTNAME# OriginSO => (#SCRIPTNAME#)base.OriginSO;

	public override void Awake(StateMachine stateMachine)
	{
	}

	public override void OnUpdate()
	{
	}

	public override void OnStateEnter()
	{
	}

	public override void OnStateExit()
	{
	}
}
